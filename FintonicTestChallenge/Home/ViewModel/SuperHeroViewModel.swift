//
//  SuperHeroViewModel.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct SuperHeroViewModel {
    
    fileprivate let hero: SuperHero
    
    init(hero: SuperHero) {
        self.hero = hero
    }
    
    var getName: String { return hero.name }
    var getPhoto: String { return hero.photo }
    var getRealName: String { return hero.realName }
    var getHeight: String { return hero.height }
    var getPower: String { return hero.power }
    var getAbilities: String { return hero.abilities }
    var getGroups: String { return hero.groups }
}
