//
//  HeroCell.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class HeroCell: UITableViewCell {
    
    var modelView: SuperHeroViewModel? {
        didSet {
            if let viewModel = modelView {
                if let url = URL(string: viewModel.getPhoto) {
                    heroIcon.downloaded(from: url)
                } else {
                    heroIcon.image = UIImage(named: "placeholder-image")
                }
                heroName.text = viewModel.getName
            }
        }
    }
    
    let heroIcon: UIImageView = {
        let heroIcon = UIImageView(frame: CGRect.zero)
        heroIcon.image = UIImage(named: "placeholder-image")
        heroIcon.contentMode = UIView.ContentMode.scaleToFill
        heroIcon.layer.cornerRadius = 25
        heroIcon.layer.masksToBounds = true
        heroIcon.translatesAutoresizingMaskIntoConstraints = false
        return heroIcon
    }()
    
    let heroName: UILabel = {
        let heroName = UILabel(frame: CGRect.zero)
        heroName.text = "Nombre del super heroe"
        heroName.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        heroName.textColor = UIColor.label
        heroName.numberOfLines = 0
        heroName.translatesAutoresizingMaskIntoConstraints = false
        return heroName
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "hero")
        
        setCell()
    }
    
    // MARK: - Método para inicializar los componentes de la celda
    private func setCell() {
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes de la celda
    private func setSubviews() {
        
        self.addSubview(heroIcon)
        self.addSubview(heroName)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            heroIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            heroIcon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            heroIcon.widthAnchor.constraint(equalToConstant: 50),
            heroIcon.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            heroName.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            heroName.leadingAnchor.constraint(equalTo: heroIcon.trailingAnchor, constant: 10),
            heroName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            heroName.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

