//
//  HomeTableExtension.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension HomeTableVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: [Codable]) {
        
        if let heroes = response as? [SuperHero] {
            dataSource = heroes
            saveHeroes(jsonArray: heroes)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                LoaderView.shared.hideActivityIndicator(uiView: self.parent!.view)
            }
        } else {
            debugPrint("Ocurrió un error en el casteo")
            LoaderView.shared.hideActivityIndicator(uiView: self.parent!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        
        debugPrint(error.localizedDescription)
        LoaderView.shared.hideActivityIndicator(uiView: self.parent!.view)
        let alert = UIAlertController(title: "Aviso", message: "No se pudo obtener la información, espere unos segundos y vuelva a intentarlo", preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    
}

extension HomeTableVC: UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    
    // MARK: - Método para validar si existe conexión a internet y realizar la acción correspondiente
    func validateNetwork() {
        if NetworkConnection.isConnectedToNetwork() {
            dataRequest()
        } else {
            getHeroes()
        }
    }
    
    // MARK: - Método para hacer la petición al servicio
    func dataRequest() {
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getRequest(endpoint: Endpoints.shared.data_endpoint)
        LoaderView.shared.showActivityIndicatory(uiView: self.parent!.view)
        self.refreshControl?.endRefreshing()
    }
    
    // MARK: - Método para hacer el filtrado del arreglo de super heroes
    func filterContent(_ searchText: String) {

        filterDataSource.removeAll()
        filterDataSource = dataSource.filter({ (hero: SuperHero) -> Bool in
            let match = hero.name.range(of: searchText, options: .caseInsensitive)

            return match != nil
        })

    }
    
    // MARK: - Método para obtener el resultado de la búsqueda de super heroes
    func updateSearchResults(for searchController: UISearchController) {
        
        if let searchText = searchController.searchBar.text {
            
            filterContent(_: searchText)
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Método para agregar el searchBarController a la navigationBar
    func setSearchBar() {

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Buscar..."
        navigationItem.searchController = searchController
        definesPresentationContext = true

    }
}

// MARK: - Extension que contiene los métodos para trabagar con la entidad HeroEntity
extension HomeTableVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveHeroes(jsonArray: [SuperHero]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: HeroEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }

    }
    
    // MARK: Método para extraer los habitantes de CoreData
    func getHeroes() {
        do {
            let data = try getData()
            
            if let heroes = data {
                dataSource = heroes
                self.tableView.reloadData()
                
            } else {
                debugPrint("No hay heroes en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        
    }
    
    func insertData(data: SuperHero) throws {
        
        let entity = HeroEntity(context: getContext)
        entity.name = data.name
        entity.photo = data.photo
        entity.realName = data.realName
        entity.height = data.height
        entity.power = data.power
        entity.abilities = data.abilities
        entity.groups = data.groups
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [SuperHero]? {
        
        var jsonArray = [SuperHero]()
        let data = try getContext.fetch(HeroEntity.fetchRequest() as NSFetchRequest<HeroEntity>)
        
        if data.count > 0 {
            for item in data {
                let hero = SuperHero(entity: item)
                jsonArray.append(hero)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
    // MARK: - Métodos para trabajar con la entidad FavoriteEntity
    func saveHero(item: SuperHero) {
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "FavoriteEntity")
        request.predicate = NSPredicate(format: "name = %@", item.name)
        
        do {
            guard let registers = try getContext.fetch(request) as? [FavoriteEntity] else {
                fatalError()
            }
            if registers.first != nil {
                debugPrint("No se puede guardar ya existe")
                let alert = UIAlertController(title: "Aviso", message: "Este super heroe ya se encuentra en favoritos", preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                present(alert, animated: true, completion: nil)
            } else {
                do {
                    try insertFavorite(data: item)
                    debugPrint("Registro insertado")
                    let alert = UIAlertController(title: "Aviso", message: "Ahora \(item.name) pertenece a tu lista de favoritos!!", preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    present(alert, animated: true, completion: nil)
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        
    }
    
    func insertFavorite(data: SuperHero) throws {
        
        let entity = FavoriteEntity(context: getContext)
        entity.name = data.name
        entity.photo = data.photo
        entity.realName = data.realName
        entity.height = data.height
        entity.power = data.power
        entity.abilities = data.abilities
        entity.groups = data.groups
        
        getContext.insert(entity)
        
        try getContext.save()
        
        debugPrint("Se almacenó el registro correctamente")
    }
}
