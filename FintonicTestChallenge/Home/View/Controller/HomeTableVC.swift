//
//  HomeTableVC.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class HomeTableVC: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var dataSource: [SuperHero] = []
    var filterDataSource: [SuperHero] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        self.navigationItem.title = "Inicio"
        self.tableView.register(HeroCell.self, forCellReuseIdentifier: "hero")
        setSearchBar()
        addRefreshControl()
        validateNetwork()
    }
    
    // MARK: - Método para agregar un refresh control a la tabla
    private func addRefreshControl() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(refreshData), for: .valueChanged)
        self.refreshControl = refreshControl
    }
    
    // MARK: - Método asociado al refresh control
    @objc private func refreshData() {
        
        validateNetwork()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchController.isActive ? filterDataSource.count : dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hero", for: indexPath) as! HeroCell
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        cell.modelView = searchController.isActive ? SuperHeroViewModel(hero: filterDataSource[indexPath.row]) : SuperHeroViewModel(hero: dataSource[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let favoriteAction = UIContextualAction(style: .normal, title: "Favorito") { (action, sourceView, completionHandler) in
            
            if self.searchController.isActive {
                self.saveHero(item: self.filterDataSource[indexPath.item])
            } else {
                self.saveHero(item: self.dataSource[indexPath.item])
            }
    
            completionHandler(true)
        }
        favoriteAction.backgroundColor = UIColor.systemGreen
        
        let actionButton = UISwipeActionsConfiguration(actions: [favoriteAction])
        actionButton.performsFirstActionWithFullSwipe = true
        
        return actionButton
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !searchController.isActive {
            let nextVC = DetailHeroVC()
            nextVC.currentHero = dataSource[indexPath.item]
            navigationController?.pushViewController(nextVC, animated: true)
        } else {
            let nextVC = DetailHeroVC()
            nextVC.currentHero = filterDataSource[indexPath.item]
            navigationController?.pushViewController(nextVC, animated: true)
        }

    }

    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !UserDefaults.standard.bool(forKey: "firstEntry") {

            let onboarding = OnboardingVC()
            self.navigationController?.present(onboarding, animated: true) {
                UserDefaults.standard.set(true, forKey: "firstEntry")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejor de memoria")
    }

}
