//
//  SuperHero.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct SuperHero: Codable {
    
    let name: String
    let photo: String
    let realName: String
    let height: String
    let power: String
    let abilities: String
    let groups: String
    
    init(entity: HeroEntity) {
        
        self.name = entity.name ?? ""
        self.photo = entity.photo ?? ""
        self.realName = entity.realName ?? ""
        self.height = entity.height ?? ""
        self.power = entity.power ?? ""
        self.abilities = entity.abilities ?? ""
        self.groups = entity.groups ?? ""
    }
    
    init(entity: FavoriteEntity) {
        
        self.name = entity.name ?? ""
        self.photo = entity.photo ?? ""
        self.realName = entity.realName ?? ""
        self.height = entity.height ?? ""
        self.power = entity.power ?? ""
        self.abilities = entity.abilities ?? ""
        self.groups = entity.groups ?? ""
    }
}

struct SuperHeroes: Codable {
    
    var superheroes: [SuperHero]
}
