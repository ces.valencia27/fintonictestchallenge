//
//  OnboardingVC.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class OnboardingVC: UIViewController {

    let UI: OnboardingView = {
        let UI = OnboardingView(frame: CGRect.zero)
        UI.backgroundColor = .systemBackground
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    let items = [
        OnboardingModel(icon_name: "sugestedIcon", title: "Universo MARVEL", description_text: "Conoce a los super heroes de Marvel"),
        OnboardingModel(icon_name: "searchIcon", title: "Búsqueda avanzada", description_text: "Encuentra de manera más rápida a los super heroes de tu interés"),
        OnboardingModel(icon_name: "favoriteIcon", title: "Favoritos", description_text: "Guarda en favoritos a los heroes que más te gusten")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setSubviews()
        setAutolayout()
        setDelegates()
        setTargets()
    }
    
    // MARK: Método para agregar los componentes a la vista principal del controlador
    private func setSubviews() {
        
        view.backgroundColor = UIColor.white
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista principal del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    // MARK: - Delegates
    private func setDelegates() {
        
        UI.onboardingCollection.delegate = self
        UI.onboardingCollection.dataSource = self
        
    }
    
    // MARK: - Método para asociar los eventos correspondientes con los botones del controlador
    private func setTargets() {
        
        UI.continueBtn.addTarget(self, action: #selector(continuePressed), for: UIControl.Event.allEvents)
    }
    
    @objc private func continuePressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejo de memoria")
    }

}
