//
//  OnboardingCell.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {
    
    var modelView: OnboardingViewModel? {
        didSet {
            if let viewModel = modelView {
                icon.image = UIImage(named: viewModel.getIconName)
                title.text = viewModel.getTitle
                preview.text = viewModel.getDescription
            } else {
                icon.image = UIImage(named: "logo")
                title.text = ""
                preview.text = ""
            }
        }
    }
    
    let icon: UIImageView = {
        let icon = UIImageView(frame: CGRect.zero)
        icon.image = UIImage(named: "logo")
        icon.contentMode = UIView.ContentMode.scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    let title: UILabel = {
        let title = UILabel(frame: CGRect.zero)
        title.text = "Instrucción"
        title.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        title.textAlignment = NSTextAlignment.left
        title.numberOfLines = 0
        title.textColor = .label
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    let preview: UILabel = {
        let preview = UILabel(frame: CGRect.zero)
        preview.text = "Texto de previsualización que contiene información sobre el título o instrucción"
        preview.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        preview.textAlignment = NSTextAlignment.left
        preview.numberOfLines = 0
        preview.textColor = .label
        preview.translatesAutoresizingMaskIntoConstraints = false
        return preview
    }()
    
    let stack: UIStackView = {
        let stack = UIStackView(frame: CGRect.zero)
        stack.axis = NSLayoutConstraint.Axis.vertical
        stack.distribution = UIStackView.Distribution.fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    // MARK: - Método para cargar los componentens de la celda
    private func setupCell() {
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes de la celda
    private func setSubviews() {
        
        self.addSubview(icon)
        stack.addArrangedSubview(title)
        stack.addArrangedSubview(preview)
        self.addSubview(stack)
        
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 35),
            icon.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
            icon.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25)
        ])
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -35),
            stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            stack.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10)
        ])
    }
}
