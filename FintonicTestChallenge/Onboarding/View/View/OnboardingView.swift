//
//  OnboardingView.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class OnboardingView: UIView {

    let logo: UIImageView = {
        let logo = UIImageView(frame: CGRect.zero)
        logo.image = UIImage(named: "logo")
        logo.contentMode = UIView.ContentMode.scaleToFill
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()
    
    let welcomeLabel: UILabel = {
        let welcomeLabel = UILabel(frame: CGRect.zero)
        welcomeLabel.text = "Conoce a los personajes de MARVEL!"
        welcomeLabel.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.black)
        welcomeLabel.textAlignment = NSTextAlignment.center
        welcomeLabel.numberOfLines = 0
        welcomeLabel.textColor = UIColor.label
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        return welcomeLabel
    }()
    
    let continueBtn: UIButton = {
        let continueBtn = UIButton(type: UIButton.ButtonType.system)
        continueBtn.setAttributedTitle(NSAttributedString(string: "Continuar", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)]), for: UIControl.State.normal)
        continueBtn.backgroundColor = UIColor.systemBlue
        continueBtn.layer.cornerRadius = 10.0
        continueBtn.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        continueBtn.layer.shadowRadius = 5.0
        continueBtn.layer.shadowColor = UIColor.black.cgColor
        continueBtn.layer.shadowOpacity = 0.5
        continueBtn.translatesAutoresizingMaskIntoConstraints = false
        return continueBtn
    }()
    
    let onboardingCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        let onboardingCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        onboardingCollection.register(OnboardingCell.self, forCellWithReuseIdentifier: "onboarding")
        onboardingCollection.backgroundColor = UIColor.clear
        onboardingCollection.allowsSelection = false
        onboardingCollection.translatesAutoresizingMaskIntoConstraints = false
        return onboardingCollection
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    // MARK: - Método para cargar los componentnes de la vista
    private func setupView() {
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(logo)
        self.addSubview(welcomeLabel)
        self.addSubview(continueBtn)
        self.addSubview(onboardingCollection)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            logo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            logo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            logo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            logo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4)
        ])
        
        NSLayoutConstraint.activate([
            welcomeLabel.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 0),
            welcomeLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25),
            welcomeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25)
        ])
        
        NSLayoutConstraint.activate([
            continueBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            continueBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -35),
            continueBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 35),
            continueBtn.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            onboardingCollection.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 10),
            onboardingCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            onboardingCollection.bottomAnchor.constraint(equalTo: continueBtn.topAnchor, constant: -20),
            onboardingCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
    }

}
