//
//  OnboardingViewModel.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct OnboardingViewModel {
    
    fileprivate let model: OnboardingModel
    
    init(model: OnboardingModel) {
        self.model = model
    }
    
    var getIconName: String { return model.icon_name }
    var getTitle: String { return model.title }
    var getDescription: String { return model.description_text }
}

