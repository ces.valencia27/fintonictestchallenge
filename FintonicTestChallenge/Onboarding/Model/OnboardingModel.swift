//
//  OnboardingModel.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct OnboardingModel {
    
    let icon_name: String
    let title: String
    let description_text: String
}
