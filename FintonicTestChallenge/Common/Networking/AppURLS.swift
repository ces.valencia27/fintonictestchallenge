//
//  AppURLS.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

public enum Environment {

    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("El archivo con extensión .plist no fue encontrado")
        }
        return dict
    }()
    
    static let baseURL: URL = {
        guard let rootURLstring = Environment.infoDictionary["ROOT_URL"] as? String else {
            fatalError("La URL Base no existe en el archivo info.plist")
        }
        guard let url = URL(string: rootURLstring) else {
            fatalError("La URL es inválida")
        }
        return url
    }()
}

import Foundation

class Endpoints {

    private static let sharedConstant = Endpoints()

    let data_endpoint: String = "/bvyob"

    public static var shared: Endpoints { return sharedConstant }
}
