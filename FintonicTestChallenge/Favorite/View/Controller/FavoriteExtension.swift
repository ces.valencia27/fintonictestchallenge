//
//  FavoriteExtension.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 25/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension FavoriteTableVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para eliminar un favorito
    func deleteFavorite(heroName: String) {
        
        let fetchRequest: NSFetchRequest<FavoriteEntity> = FavoriteEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate.init(format: "name = %@", heroName)

        do {
            let objects = try getContext.fetch(fetchRequest)
            for object in objects {
                getContext.delete(object)
            }
            try getContext.save()
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer los heroes favoritos de CoreData
    func getFavorites() {

        do {
            let data = try getData()

            if let heroes = data {
                dataSource = heroes
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                debugPrint("No hay registros en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func getData() throws -> [SuperHero]? {

        var jsonArray = [SuperHero]()
        let data = try getContext.fetch(FavoriteEntity.fetchRequest() as NSFetchRequest<FavoriteEntity>)

        if data.count > 0 {
            for item in data {
                let model = SuperHero(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
