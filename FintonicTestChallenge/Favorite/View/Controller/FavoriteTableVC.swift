//
//  FavoriteTableVC.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class FavoriteTableVC: UITableViewController {
    
    var dataSource: [SuperHero] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        self.navigationItem.title = "Favoritos"
        self.tableView.register(HeroCell.self, forCellReuseIdentifier: "hero")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hero", for: indexPath) as! HeroCell
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        cell.modelView = SuperHeroViewModel(hero: dataSource[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let favoriteAction = UIContextualAction(style: .destructive, title: "Eliminar") { (action, sourceView, completionHandler) in
            
            let alert = UIAlertController(title: "Aviso", message: "¿Estás seguro que deseas eliminar a este heroe de favoritos?", preferredStyle: UIAlertController.Style.alert)
            let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive, handler: nil)
            let deleteAction = UIAlertAction(title: "Eliminar", style: UIAlertAction.Style.default) { (_) in
                self.deleteFavorite(heroName: self.dataSource[indexPath.item].name)
                
                self.dataSource.remove(at: indexPath.item)
                self.tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            alert.addAction(cancelAction)
            alert.addAction(deleteAction)
            self.present(alert, animated: true, completion: nil)
    
            completionHandler(true)
        }
        favoriteAction.backgroundColor = UIColor.systemRed
        
        let actionButton = UISwipeActionsConfiguration(actions: [favoriteAction])
        actionButton.performsFirstActionWithFullSwipe = true
        
        return actionButton
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextVC = DetailHeroVC()
        nextVC.currentHero = dataSource[indexPath.item]
        navigationController?.pushViewController(nextVC, animated: true)
        
    }

    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {
        
        getFavorites()
    }
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejor de memoria")
    }

}
