//
//  DetailHeroCell.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class DetailHeroCell: UICollectionViewCell {
    
    var modelView: DetailHeroViewModel? {
        didSet {
            if let viewModel = modelView {
                let tagName = NSAttributedString(string: "\(viewModel.getTag): ", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.label])
                let contentText = NSAttributedString(string: viewModel.getDetail, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.label])
                let fullText = NSMutableAttributedString()
                fullText.append(tagName)
                fullText.append(contentText)
                
                detailContent.attributedText = fullText
            }
        }
    }
    
    let detailContent: UILabel = {
        let tagName = NSAttributedString(string: "TagName: ", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.label])
        let contentText = NSAttributedString(string: "Contenido", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.label])
        let fullText = NSMutableAttributedString()
        fullText.append(tagName)
        fullText.append(contentText)
        
        let detailContent = UILabel(frame: CGRect.zero)
        detailContent.attributedText = fullText
        detailContent.textColor = UIColor.label
        detailContent.numberOfLines = 0
        detailContent.translatesAutoresizingMaskIntoConstraints = false
        return detailContent
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setCell()
    }
    
    // MARK: - Método para inicializar los componentes de la celda
    private func setCell() {
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes de la celda
    private func setSubviews() {
        
        self.addSubview(detailContent)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            detailContent.topAnchor.constraint(equalTo: self.topAnchor),
            detailContent.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            detailContent.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
    }
}
