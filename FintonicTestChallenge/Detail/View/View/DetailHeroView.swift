//
//  DetailHeroView.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class DetailHeroView: UIView {

    var modelView: SuperHero? {
        didSet {
            if let viewModel = modelView {
                if let url = URL(string: viewModel.photo) {
                    heroImage.downloaded(from: url)
                } else {
                    heroImage.image = UIImage(named: "placeholder-image")
                }
                heroName.text = viewModel.name
            }
        }
    }
    
    let scrollContainer: UIScrollView = {
        let scrollContainer = UIScrollView(frame: CGRect.zero)
        scrollContainer.backgroundColor = UIColor.systemBackground
        scrollContainer.isPagingEnabled = true
        scrollContainer.translatesAutoresizingMaskIntoConstraints = false
        return scrollContainer
    }()
    
    let heroImage: UIImageView = {
        let heroImage = UIImageView(frame: CGRect.zero)
        heroImage.image = UIImage(named: "placeholder-image")
        heroImage.contentMode = UIView.ContentMode.scaleToFill
        heroImage.translatesAutoresizingMaskIntoConstraints = false
        return heroImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.7)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let heroName: UILabel = {
        let heroName = UILabel(frame: CGRect.zero)
        heroName.text = "Nombre del super heroe"
        heroName.textColor = UIColor.label
        heroName.font = UIFont.systemFont(ofSize: 35, weight: UIFont.Weight.bold)
        heroName.textAlignment = NSTextAlignment.center
        heroName.numberOfLines = 0
        heroName.translatesAutoresizingMaskIntoConstraints = false
        return heroName
    }()
    
    let slideText: UILabel = {
        let slideText = UILabel(frame: CGRect.zero)
        slideText.text = "Desliza para ver más información"
        slideText.textColor = UIColor.label
        slideText.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        slideText.textAlignment = NSTextAlignment.center
        slideText.numberOfLines = 0
        slideText.translatesAutoresizingMaskIntoConstraints = false
        return slideText
    }()
    
    let botContainer: UIView = {
        let botContainer = UIView(frame: CGRect.zero)
        botContainer.backgroundColor = UIColor.systemBackground
        botContainer.translatesAutoresizingMaskIntoConstraints = false
        return botContainer
    }()
    
    let detailsCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        
        let detailsCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        detailsCollection.register(DetailHeroCell.self, forCellWithReuseIdentifier: "detail")
        detailsCollection.backgroundColor = UIColor.systemBackground
        detailsCollection.isScrollEnabled = false
        detailsCollection.translatesAutoresizingMaskIntoConstraints = false
        return detailsCollection
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    // MARK: - Método para inicializar los componentes de la vista
    private func setupView() {
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes de la vista
    private func setSubviews() {
        
        self.addSubview(scrollContainer)
        scrollContainer.addSubview(heroImage)
        scrollContainer.addSubview(overlay)
        overlay.addSubview(heroName)
        overlay.addSubview(slideText)
        scrollContainer.addSubview(botContainer)
        botContainer.addSubview(detailsCollection)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scrollContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            scrollContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            scrollContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            heroImage.topAnchor.constraint(equalTo: scrollContainer.topAnchor),
            heroImage.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor),
            heroImage.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 1)
        ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: scrollContainer.topAnchor),
            overlay.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor),
            overlay.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 1)
        ])
        
        NSLayoutConstraint.activate([
            heroName.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40),
            heroName.centerYAnchor.constraint(equalTo: overlay.centerYAnchor),
            heroName.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40)
        ])
        
        NSLayoutConstraint.activate([
            slideText.bottomAnchor.constraint(equalTo: overlay.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            slideText.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -30),
            slideText.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 30)
        ])
        
        NSLayoutConstraint.activate([
            botContainer.topAnchor.constraint(equalTo: overlay.bottomAnchor),
            botContainer.widthAnchor.constraint(equalTo: scrollContainer.widthAnchor),
            botContainer.heightAnchor.constraint(equalTo: scrollContainer.heightAnchor, multiplier: 1),
            botContainer.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            detailsCollection.topAnchor.constraint(equalTo: botContainer.safeAreaLayoutGuide.topAnchor, constant: 20),
            detailsCollection.trailingAnchor.constraint(equalTo: botContainer.trailingAnchor, constant: -25),
            detailsCollection.bottomAnchor.constraint(equalTo: botContainer.bottomAnchor, constant: 0),
            detailsCollection.leadingAnchor.constraint(equalTo: botContainer.leadingAnchor, constant: 25)
        ])
        
    }

}
