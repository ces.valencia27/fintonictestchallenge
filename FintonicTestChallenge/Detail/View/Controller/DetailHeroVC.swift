//
//  DetailHeroVC.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class DetailHeroVC: UIViewController {

    let UI: DetailHeroView = {
        let UI = DetailHeroView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.systemBackground
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var currentHero: SuperHero?
    var detailArray: [DetailHeroModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        loadUI()

    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func loadUI() {
        
        setSubviews()
        setAutolayout()
        setDelegates()
        setArray()
    }
    
    // MARK: - Método para agregar los componentes de la vista principal del controlador
    private func setSubviews() {
        
        self.navigationItem.largeTitleDisplayMode = .never
        self.title = "Detalle"
        view.addSubview(UI)
        UI.modelView = currentHero
        
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    // MARK: - Método para asignar los delegados de los protocolos
    private func setDelegates() {
        
        UI.detailsCollection.delegate = self
        UI.detailsCollection.dataSource = self
    }
    
    // MARK: - Método para inicializar el arreglo de detalle
    private func setArray() {
        
        detailArray = [
            DetailHeroModel(detailTag: "Nombre real", detailContent: currentHero?.realName ?? ""),
            DetailHeroModel(detailTag: "Altura", detailContent: currentHero?.height ?? ""),
            DetailHeroModel(detailTag: "Poder", detailContent: currentHero?.power ?? ""),
            DetailHeroModel(detailTag: "Habilidades", detailContent: currentHero?.abilities ?? ""),
            DetailHeroModel(detailTag: "Grupos", detailContent: currentHero?.groups ?? "")
        ]
        
        DispatchQueue.main.async {
            self.UI.detailsCollection.reloadData()
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejo de memoria")
    }

}
