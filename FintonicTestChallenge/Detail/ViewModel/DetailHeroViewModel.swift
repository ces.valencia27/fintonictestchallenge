//
//  DetailHeroViewModel.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct DetailHeroViewModel {
    
    fileprivate let model: DetailHeroModel
    
    init(model: DetailHeroModel) {
        self.model = model
    }
    
    var getTag: String { return model.detailTag }
    var getDetail: String { return model.detailContent }
}
