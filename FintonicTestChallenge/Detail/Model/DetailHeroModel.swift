//
//  DetailHeroModel.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import Foundation

struct DetailHeroModel {
    
    let detailTag: String
    let detailContent: String
}
