//
//  HomeTabBar.swift
//  FintonicTestChallenge
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import UIKit

class HomeTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configurando la tabBar
        setupTabBar()
    }
    
    // MARK: - Método para configurar la tabBar
    func setupTabBar() {

        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        
        let home = HomeTableVC()
        let navigationHome = UINavigationController(rootViewController: home)
        navigationHome.navigationBar.prefersLargeTitles = true
        navigationHome.navigationBar.scrollEdgeAppearance = appearance
        
        let vc1 = navigationHome
        let item1 = UITabBarItem(tabBarSystemItem: UITabBarItem.SystemItem.history, tag: 1)
        vc1.tabBarItem = item1
        
        let favorites = FavoriteTableVC()
        let navigationFavorites = UINavigationController(rootViewController: favorites)
        navigationFavorites.navigationBar.prefersLargeTitles = true
        navigationFavorites.navigationBar.scrollEdgeAppearance = appearance
        
        let vc2 = navigationFavorites
        let item2 = UITabBarItem(tabBarSystemItem: UITabBarItem.SystemItem.favorites, tag: 2)
        vc2.tabBarItem = item2
        
        self.setViewControllers([vc1, vc2], animated: true)
        self.tabBar.tintColor = UIColor.systemBlue
        self.tabBar.unselectedItemTintColor = UIColor.lightGray
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejo de memoria")
    }

}

