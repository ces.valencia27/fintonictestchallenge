# FintonicTestChallenge 

Prueba técnica para Fintonic

- Se utilizó MVVM como arquitectura al ser un proyecto pequeño y para facilitar los casos de pruebas unitarias.
- Se implementó Core Data para la persistencia de datos de la aplicación.
- Se agregaron botones de acción en las tablas para agregar a favoritos y para eliminar de favoritos.
- Se agregó un refresh Control para poder hacer la petición al servicio y actualizar la información en caso de que se agregaran nuevos super héroes.
- Se utilizó un archivo de configuración que contiene la URL Base del servicio que se consume. Regularmente así manejo diferentes ambientes.
- Se agregaron comentarios simples pero descriptivos en los métodos creados en todo el proyecto.
- El proyecto está habilitado para trabajar tanto con Dark Mode como Light Mode.
- Todas las vistas están creadas con código, el autolayout es programático para tener un mayor control y mejor organización de las vistas.
- En Core Data se utilizó un método genérico para eliminar cualquier entidad, es por eso que se utilizaron 2 entidades exactamente iguales.
- El diseño de la interfaz se construyo basado en Human Interface Guidelines esto con el fin de lograr una experiencia de usuario óptima.
- Se implementó Singleton para crear una instancia única que permitiera acceder al Contexto, esto en el manejo de CoreData.
- El cliente para el consumo del servicio es nativo utilizando URL_Session y se creó un protocolo para el manejo de las respuestas.
- Se realizaron algunos casos de pruebas unitarias básicos para validar el funcionamiento del viewmodel y del modelo.
