//
//  FintonicTestChallengeTests.swift
//  FintonicTestChallengeTests
//
//  Created by David Valencia on 24/10/19.
//  Copyright © 2019 Cesar David Valencia Estrada. All rights reserved.
//

import XCTest
@testable import FintonicTestChallenge

class FintonicTestChallengeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // Casos de prueba
    func testHeroViewModel() {
        
        let jsonStr = "{\"name\": \"Spiderman\",\"photo\": \"https://i.annihil.us/u/prod/marvel/i/mg/9/30/538cd33e15ab7/standard_xlarge.jpg\",\"realName\": \"Peter Benjamin Parker\",\"height\": \"1.77m\",\"power\": \"Peter can cling to most surfaces, has superhuman strength (able to lift 10 tons optimally) and is roughly 15 times more agile than a regular human.\",\"abilities\": \"Peter is an accomplished scientist, inventor and photographer.\",\"groups\": \"Avengers, formerly the Secret Defenders, \"New Fantastic Four\", the Outlaws\"}"
        
        if let jsonData = jsonStr.data(using: String.Encoding.utf8) {
            let decoder = JSONDecoder()

            if let hero = try? decoder.decode(SuperHero.self, from: jsonData) {
                let interface = HeroCell()
                let viewModel = SuperHeroViewModel(hero: hero)
                interface.modelView = viewModel
                
                XCTAssertEqual(hero.name, interface.heroName.text)
                
            } else {
                debugPrint("Error en la serialización del json")
            }
        } else {
            debugPrint("Error al desempaquetar el valor")
        }
        
    }
    
    func testHeroViewModel2() {
        
        let json: NSDictionary = ["name":"Spiderman", "photo":"https://", "realName":"Peter parker", "height":"1.76m", "power":"Telaraña", "abilities":"Cientifico", "groups":"No tiene"]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let decoder = JSONDecoder()

        if let hero = try? decoder.decode(SuperHero.self, from: jsonData) {
            let interface = HeroCell()
            let viewModel = SuperHeroViewModel(hero: hero)
            interface.modelView = viewModel
            
            debugPrint(hero.name)
            
            XCTAssertEqual(hero.name, interface.heroName.text)
            
        } else {
            debugPrint("Error en la serialización del json")
        }
        
    }
    
    func testHeroViewModel3() {
        
        let json: NSDictionary = ["name":"Spiderman", "photo":"https://", "realName":"Peter parker", "height":1.75, "power":"Telaraña", "abilities":"Cientifico", "groups":"No tiene"]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let decoder = JSONDecoder()

        if let hero = try? decoder.decode(SuperHero.self, from: jsonData) {
            
            debugPrint(hero.height)
            
            XCTAssertEqual(hero.height, "hola")
            
        } else {
            debugPrint("Error en la serialización del json")
        }
        
    }


}
